// Import express and request modules
var express = require("express");
var request = require("request");
var bodyParser = require("body-parser");
var _ = require("underscore");

// Store our app's ID and Secret. These we got from Step 1.
// For this tutorial, we'll keep your API credentials right here. But for an actual app, you'll want to  store them securely in environment variables.
const CLIENT_ID = "333636330625.482012005696";
const CLIENT_SECRET = "bc315892798d2c80521c0ce9357ec635";
const OAUTH_ACCESS_TOKEN = "xoxp-333636330625-333457469120-482775655506-ec3799490a922dc7cc919711e4850392";

const OAUTH_API_URL = "https://slack.com/api/oauth.access";
const CHANNEL_INFO_API_URL = "https://slack.com/api/channels.info";
const USER_INFO_API_URL = "https://slack.com/api/users.info";
// Again, we define a port we want to listen to
const PORT = 4390;

// Instantiates Express and assigns our app variable to it
var app = express();
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// Lets start our server
app.listen(PORT, function() {
  //Callback triggered when server is successfully listening. Hurray!
  console.log("Example app listening on port " + PORT);
});

// This route handles GET requests to our root ngrok address and responds with the same "Ngrok is working message" we used before
app.get("/", function(req, res) {
  res.send("Ngrok is working! Path Hit: " + req.url);
});

// This route handles get request to a /oauth endpoint. We'll use this endpoint for handling the logic of the Slack oAuth process behind our app.
app.get("/oauth", function(req, res) {
  // When a user authorizes an app, a code query parameter is passed on the oAuth endpoint. If that code is not there, we respond with an error message
  if (!req.query.code) {
    res.status(500);
    res.send({ Error: "Looks like we're not getting code." });
    console.log("Looks like we're not getting code.");
  } else {
    // If it's there...

    // We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID, client secret, and the code we just got as query parameters.
    let qs = {
      code: req.query.code,
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
    };
    request({ url: OAUTH_API_URL, qs: qs }, function(error, response, body) {
      if (error) {
        console.log(error);
        return;
      }
      console.log("Oauth body");
      console.log(body);
      res.json(body);
    });
  }
});

// Route the endpoint that our slash command will point to and send back a simple response to indicate that ngrok is working
app.post("/cr", function(req, res) {
  // res.send("Your ngrok tunnel is up and running!");
  let channel = req.body["channel_id"];
  let thisUser = req.body["user_id"];
  let gerritUrl = req.body["text"];
  let channelHook = res;

  let qs = {
    token: OAUTH_ACCESS_TOKEN,
    channel: channel,
  };
  request.get({ url: CHANNEL_INFO_API_URL, qs: qs }, function(err, response, body) {
    if (err) {
      console.log(err);
      return;
    }
    // Got the channel info!

    // Let's take out the users, subtract the current user, and pick a random one to do our CR
    let usersInChannel = JSON.parse(body)["channel"]["members"];
    let otherUsersInChannel = _.without(usersInChannel, thisUser);
    let theChosenUser = _.sample(otherUsersInChannel);

    channelHook.send(`Could I get a CR please <@${theChosenUser}>? ${gerritUrl}`);
  });
});
